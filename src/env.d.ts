declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: string;
    VUE_ROUTER_MODE: 'hash' | 'history' | 'abstract' | undefined;
    VUE_ROUTER_BASE: string | undefined;
    API_BASE: string;
    OAUTH_BASE: string;
    OAUTH_CLIENT_ID: string;
    OAUTH_REDIRECT_URL: string;
  }
}
