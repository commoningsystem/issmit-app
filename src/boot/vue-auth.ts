
import { boot } from 'quasar/wrappers'
import axios from 'axios'
import {createAuth}          from '@websanova/vue-auth';
import driverAuthBearer from '@websanova/vue-auth/dist/drivers/auth/bearer.esm';
import driverHttpAxios       from '@websanova/vue-auth/dist/drivers/http/axios.1.x.esm';
import driverRouterVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm';
//import driverOAuth2Google    from '@websanova/vue-auth/dist/drivers/oauth2/google.esm';
//import driverOAuth2Facebook  from '@websanova/vue-auth/dist/drivers/oauth2/facebook.esm.js';

// auth driver
const driverOAuth2Backend = {
  url: 'http://localhost:8000/o/authorize',

    params: {
        client_id: 'MoqmEMqvcHiangx56AnOPaQTrvJueHkvobbo3nkS',
        // redirect_uri: 'login/backend',
        response_type: 'code',
        scope: 'read write',
        state: {},
    }
}

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot( ( { app, router, } ) => {
  const auth = createAuth({
    plugins: {
        http: axios,
        router: router
    },
    drivers: {
        http: driverHttpAxios,
        auth: driverAuthBearer,
        router: driverRouterVueRouter,
        oauth2: {
            backend: driverOAuth2Backend,
            //facebook: driverOAuth2Facebook,
        }
    },
    options: {
        rolesKey: 'type',
        notFoundRedirect: {name: 'user-account'},
    }
  });

  app.use(auth);
})
