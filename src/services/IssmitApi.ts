import { api } from 'src/boot/axios';
import { Event } from 'src/components/models';
import { useStore } from 'src/store';

export default {
  getHeaders() {
    const store = useStore();
    const accessToken = store.state.auth.accessToken;
    let headers = {};
    if (accessToken) {
      headers = {
        Authorization: `Bearer ${accessToken}`,
      };
    }
    return headers;
  },
  getEvents() {
    const headers = this.getHeaders()

    return api
      .get<Event[]>('/api/v1/events/', {
        headers: headers,
      })
      .then((response) => {
        return response.data;
      });
  },
  getEvent(id: string) {
    const headers = this.getHeaders()

    return api
      .get<Event>(`/api/v1/events/${id}/`, {
        headers: headers,
      })
      .then((response) => {
        return response.data;
      });
  },
};
