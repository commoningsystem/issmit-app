export default {
  // c LoginFormComponent
  pageLoginTitle: 'Log into Issmit App',
  email: 'email',
  password: 'password',
  login: 'login',
  oauthLogin: 'Oauth login',
  oauthLogout: 'Oauth logout',
};
