import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';

const getters: GetterTree<AuthStateInterface, StateInterface> = {
  // get accessToken(): string {
  //   return this.state.accessToken
  // }
  accessToken(state) {
    return state.accessToken
  }
};

export default getters;
