import { MutationTree } from 'vuex';
import { AuthStateInterface } from './state';

const mutation: MutationTree<AuthStateInterface> = {
  SET_ACCESS_TOKEN(state: AuthStateInterface, accessToken: string) {
    state.accessToken = accessToken
  },

  SET_USERNAME(state: AuthStateInterface, username: string) {
    state.username = username
  }
};

export default mutation;
