import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';

const actions: ActionTree<AuthStateInterface, StateInterface> = {
  setAccessToken(context, accessToken) {
    context.commit('SET_ACCESS_TOKEN', accessToken) 
  },
  setUsername(context, username) {
    context.commit('SET_USERNAME', username) 
  }
};

export default actions;
