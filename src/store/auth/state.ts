export interface AuthStateInterface {
  accessToken: string;
  username: string;
}

function state(): AuthStateInterface {
  return {
    accessToken: '',
    username: '',
  };
}

export default state;
