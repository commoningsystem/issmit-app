import {VueAuth} from 'src/types/websanova__vue-auth';
import { InjectionKey } from 'vue';

// types for providers
export const AuthKey: InjectionKey<VueAuth>= Symbol('VueAuth');
