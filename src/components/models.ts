export interface Event {
  id: number
  takes_place_on: Date
  takes_place_at: string
  name: string
  description: string
  banner: string
  max_participants_count: number
  max_local_participants_count: number
  max_pickup_participants_count: number
  pickup_time: Date
  contributors: any[]
  participants: any[]
  pattern: any
  host: any
}
export interface Todo {
  id: number;
  content: string;
}

export interface Meta {
  totalCount: number;
}
