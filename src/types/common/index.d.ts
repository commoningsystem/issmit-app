declare module '@websanova/vue-auth/dist/drivers/auth/bearer.esm' {
    type driverAuthBearer = Record<string, unknown>
}

declare module '@websanova/vue-auth/dist/drivers/http/axios.1.x.esm' {
    type driverHttpAxios = Record<string, unknown>
}

declare module '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm' {
    type driverRouterVueRouter = Record<string, unknown>
}

declare module '@websanova/vue-auth/dist/drivers/oauth2/google.esm' {
    type driverOAuth2Google = Record<string, unknown>
}
