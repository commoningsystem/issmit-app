import { RouteRecordRaw } from 'vue-router';

// type defs to add meta options for auth
interface RouteMeta {
  auth: boolean;
}

type RouteRecordRawApp = RouteRecordRaw & { meta?: RouteMeta };

const routes: RouteRecordRawApp[] = [
  {
    path: '/',
    component: () => import('layouts/LandingLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Welcome.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/signup', component: () => import('pages/Signup.vue') },
      { path: '/events', component: () => import('pages/Event.vue') },
      { path: '/events/:id', name: 'event', component: () => import('pages/EventDetail.vue') },
    ],
  },
  {
    path: '/app',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Index.vue') }],
    meta: {
      auth: true,
    },
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
