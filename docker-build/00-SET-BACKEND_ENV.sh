#!/bin/sh

ESCAPED_API_BASE=$(printf '%s\n' "$API_BASE" | sed -e 's/[\/&]/\\&/g')
ESCAPED_OAUTH_BASE=$(printf '%s\n' "$OAUTH_BASE" | sed -e 's/[\/&]/\\&/g')
ESCAPED_OAUTH_CLIENT_ID=$(printf '%s\n' "$OAUTH_CLIENT_ID" | sed -e 's/[\/&]/\\&/g')
ESCAPED_OAUTH_REDIRECT_URL=$(printf '%s\n' "$OAUTH_REDIRECT_URL" | sed -e 's/[\/&]/\\&/g')
grep -rl "##API_BASE##" /usr/share/nginx/html | xargs sed -i "s/##API_BASE##/${ESCAPED_API_BASE}/g"
grep -rl "##OAUTH_BASE##" /usr/share/nginx/html | xargs sed -i "s/##OAUTH_BASE##/${ESCAPED_OAUTH_BASE}/g"
grep -rl "##OAUTH_CLIENT_ID##" /usr/share/nginx/html | xargs sed -i "s/##OAUTH_CLIENT_ID##/${ESCAPED_OAUTH_CLIENT_ID}/g"
grep -rl "##OAUTH_REDIRECT_URL##" /usr/share/nginx/html | xargs sed -i "s/##OAUTH_REDIRECT_URL##/${ESCAPED_OAUTH_REDIRECT_URL}/g"

