# Issmit (issmit-app)

Application to share food and coordinate cooking

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
npx quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
npx quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Notes on contribution

- Contributions are very much welcome! Please create a merge request for review of the core team. Document what your feature or bugfix is intended to do and mind the following notes.
- Stack: VueJs/Quasar/npm/SCSS
- This project is written in Typescript. If you are familiar with Javascript ES2015++ it will just add static typeing to the code to ease debugging and prevent runtime errors.
- ESLint with prettier is used. Contributions need to pass this linting and if you use an appropriate IDE your code will get formated automatically
- axios library is used for easy calls to the backend api
- application state may NOT be managed in components and components may not call the api. For this vuex is used.
- Components should either be dependent or independent and never mixed
  - dependent: state gets injected by parent component by one-way or two-way data bindings
  - independent: state is read from vuex

## ENV

ENV variables are set in .env in the root folder.

**API_BASE**

Base URL for the API.

**OAUTH_BASE**

Base URL for the OAuth endpoint, e.g. ``http://localhost:8000/o``

**OAUTH_CLIENT_ID**

OAuth client ID

**OAUTH_REDIRECT_URL**

Redirect URL for the OAuth flow, e.g. ``http://localhost:8080/``
