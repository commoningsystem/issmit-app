FROM nginx:mainline-alpine

COPY dist/spa /usr/share/nginx/html

# functionality to replace env vars in build bundle
COPY docker-build/00-SET-BACKEND_ENV.sh /docker-entrypoint.d/00-SET-BACKEND_ENV.sh
RUN chmod a+x /docker-entrypoint.d/00-SET-BACKEND_ENV.sh
